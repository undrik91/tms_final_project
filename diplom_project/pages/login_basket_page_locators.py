from selenium.webdriver.common.by import By


class LoginBasketPageLocators:
    INPUT_EMAIL = (By.XPATH, '//input[@name="email"]')
    INPUT_PASS = (By.XPATH, '//input[@name="password"]')
    BUTTON_LOGIN = (By.XPATH, '//button[@name="login"]')
    SELECT_DUCK = (By.XPATH, '//div[@id="box-most-popular"]/div/ul/li/a'
                             '[@class="link"][@title="Blue Duck"]')
    COUNT_DUCK = (By.XPATH, '//input[@type="number"]')
    BUTTON_ADD_CART = (By.XPATH, '//button[@type="submit"]')
    BUTTON_CART = (By.XPATH, '//span[@class="quantity"]')
    PRICE = (By.XPATH, '//td[@class="unit-cost"]')
    COUNT = (By.XPATH, '//td[@style="text-align: center;"]')
    BUTTON_CONFIRM = (By.XPATH, '//button[@value="Confirm Order"]')
