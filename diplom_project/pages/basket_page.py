from diplom_project.pages.base_page import BasePage
from diplom_project.pages.basket_page_locators import\
    BasketPageLocators


class BasketPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login_email(self, arg="undrik91@gmail.com"):
        button = self.driver.find_element(
            *BasketPageLocators.INPUT_EMAIL)
        button.send_keys(arg)

    def login_pass(self, arg="123"):
        button = self.driver.find_element(
            *BasketPageLocators.INPUT_PASS)
        button.send_keys(arg)

    def login_button(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_LOGIN)
        button.click()

    def select_duck(self):
        button = self.driver.find_element(
            *BasketPageLocators.SELECT_DUCK)
        button.click()

    def count_duck(self, arg):
        button = self.driver.find_element(
            *BasketPageLocators.COUNT_DUCK)
        button.clear()
        button.send_keys(arg)

    def button_add_cart(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_ADD_CART)
        button.click()

    def open_cart(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_CART)
        button.click()

    def sum_price(self):
        return self.driver.find_element(
            *BasketPageLocators.SUM_PRICE)

    def count(self):
        return self.driver.find_element(
            *BasketPageLocators.COUNT)

    def update(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_UPDATE)
        button.click()

    def remove(self):
        button = self.driver.find_element(
            *BasketPageLocators.BUTTON_REMOVE)
        button.click()

    def empty(self):
        return self.driver.find_element(
            *BasketPageLocators.EMPTY)
