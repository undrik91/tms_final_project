from diplom_project.pages.base_page import BasePage
from diplom_project.pages.regional_page_locators import\
    RegionalPageLocators


class RegionalPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_regional_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.REGIONAL_BUTTON)
        button.click()

    def currency_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.CURRENCY_BUTTON)
        button.click()

    def country_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.COUNTRY_BUTTON)
        button.click()

    def save_button(self):
        button = self.driver.find_element(
            *RegionalPageLocators.SAVE_BUTTON)
        button.click()

    def ui_currency(self):
        return self.driver.find_element(
            *RegionalPageLocators.UI_CURRENCY)

    def ui_country(self):
        return self.driver.find_element(
            *RegionalPageLocators.UI_COUNTRY)
