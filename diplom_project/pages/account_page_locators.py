from selenium.webdriver.common.by import By


class AccountPageLocators:
    INPUT_EMAIL = (By.XPATH, '//input[@name="email"]')
    INPUT_PASS = (By.XPATH, '//input[@name="password"]')
    BUTTON_LOGIN = (By.XPATH, '//button[@name="login"]')
    EDIT_ACCOUNT = (By.XPATH, '//div[@id="box-account"]/div/ul/li[3]/a')
    INPUT_NAME = (By.XPATH, '//input[@name="firstname"]')
    BUTTON_SUBMIT = (By.XPATH, '//button[@type="submit"]')
