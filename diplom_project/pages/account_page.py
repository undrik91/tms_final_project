from diplom_project.pages.base_page import BasePage
from diplom_project.pages.account_page_locators import\
    AccountPageLocators


class AccountPage(BasePage):

    URL = "/litecart"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def login_email(self, arg="undrik91@gmail.com"):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_EMAIL)
        button.send_keys(arg)

    def login_pass(self, arg="123"):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_PASS)
        button.send_keys(arg)

    def login_button(self):
        button = self.driver.find_element(
            *AccountPageLocators.BUTTON_LOGIN)
        button.click()

    def edit_account(self):
        button = self.driver.find_element(
            *AccountPageLocators.EDIT_ACCOUNT)
        button.click()

    def input_name(self, arg):
        button = self.driver.find_element(
            *AccountPageLocators.INPUT_NAME)
        button.clear()
        button.send_keys(arg)

    def button_save(self):
        button = self.driver.find_element(
            *AccountPageLocators.BUTTON_SUBMIT)
        button.click()
