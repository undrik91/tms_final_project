from selenium.webdriver.common.by import By


class RegionalPageLocators:
    REGIONAL_BUTTON = (By.XPATH, '//a[@class="fancybox-region"]')
    CURRENCY_BUTTON = (By.XPATH, '//select[@name="currency_code"]/option[3]')
    COUNTRY_BUTTON = (By.XPATH, '//select[@name="country_code"]/option[7]')
    SAVE_BUTTON = (By.XPATH, '//button[@name="save"]')
    UI_CURRENCY = (By.XPATH, '//div[@class="currency"]')
    UI_COUNTRY = (By.XPATH, '//div[@class="country"]')
