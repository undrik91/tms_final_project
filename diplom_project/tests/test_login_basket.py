import allure
import time
import mysql.connector
from diplom_project.pages.login_basket_page import LoginBasketPage


@allure.story("TC_2_price")
def test_login_basket_price(driver):
    with allure.step('open app'):
        login = LoginBasketPage(driver)
        login.open_page()
    with allure.step('loging'):
        login.login_email()
        login.login_pass()
        login.login_button()
    with allure.step('add 3 duck in basket'):
        login.select_duck()
        login.count_duck("3")
        login.button_add_cart()
        time.sleep(1)   # animation
    with allure.step('Check price is correct'):
        login.open_cart()
        assert login.price().text == "$20.00"
        login.confirm()


@allure.story("TC_2_count")
def test_login_basket(driver):
    with allure.step('open app'):
        login = LoginBasketPage(driver)
        login.open_page()
    with allure.step('loging'):
        login.login_email()
        login.login_pass()
        login.login_button()
    with allure.step('add 3 duck in basket'):
        login.select_duck()
        login.count_duck("3")
        login.button_add_cart()
        time.sleep(1)   # animation
    with allure.step('Check ducks have been added is correct'):
        login.open_cart()
        assert login.count().text == "3"
        login.confirm()


@allure.story("TC_2_bd")
def test_login_basket_bd(driver):
    with allure.step('Check BD'):
        db = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="",
            database="litecart"
        )
        cursor = db.cursor()
        query = "SELECT customer_firstname, date_created, weight_total," \
                " payment_due FROM lc_orders ORDER BY date_created" \
                " DESC LIMIT 1"
        cursor.execute(query)
        assert cursor.fetchall()[0][0] == "Andrei"
