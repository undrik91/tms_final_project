import allure
import mysql.connector
from diplom_project.pages.account_page import AccountPage


@allure.story("TC_3_rename")
def test_account_rename(driver):
    with allure.step('open app'):
        account = AccountPage(driver)
        account.open_page()
    with allure.step('loging'):
        account.login_email()
        account.login_pass()
        account.login_button()
    with allure.step('open edit account and rename'):
        account.edit_account()
        account.input_name("Andrei")
        account.button_save()
    with allure.step('Check BD'):
        db = mysql.connector.connect(
            host="127.0.0.1",
            user="root",
            passwd="",
            database="litecart"
        )
        cursor = db.cursor()
        query = "SELECT firstname FROM lc_customers where firstname='Andrei'"
        cursor.execute(query)
        assert cursor.fetchall()[0][0] == "Andrei"
