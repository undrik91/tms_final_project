import allure
import time
from diplom_project.pages.basket_page import BasketPage


@allure.story("TC_4_price")
def test_basket_price(driver):
    with allure.step('open app'):
        basket = BasketPage(driver)
        basket.open_page()
    with allure.step('loging'):
        basket.login_email()
        basket.login_pass()
        basket.login_button()
    with allure.step('add duck in basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(1)  # animation
    with allure.step('replace 1 to 3 duck in basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(1)  # animation
    with allure.step('Check price is correct'):
        assert basket.sum_price().text == "$60.00"


@allure.story("TC_4_count")
def test_basket_count(driver):
    with allure.step('open app'):
        basket = BasketPage(driver)
        basket.open_page()
    with allure.step('loging'):
        basket.login_email()
        basket.login_pass()
        basket.login_button()
    with allure.step('add duck in basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(1)  # animation
    with allure.step('replace 1 to 3 duck in basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(1)  # animation
    with allure.step('Check ducks have been added is correct'):
        assert basket.count().text == "3"


@allure.story("TC_4_remove")
def test_basket_remove(driver):
    with allure.step('open app'):
        basket = BasketPage(driver)
        basket.open_page()
    with allure.step('loging'):
        basket.login_email()
        basket.login_pass()
        basket.login_button()
    with allure.step('add duck in basket'):
        basket.select_duck()
        basket.button_add_cart()
        time.sleep(1)  # animation
    with allure.step('replace 1 to 3 duck in basket'):
        basket.open_cart()
        basket.count_duck("3")
        basket.update()
        time.sleep(1)  # animation
    with allure.step('delete and check empty basket'):
        basket.remove()
        driver.implicitly_wait(10)
        assert basket.empty().text == "There are no items in your cart."
