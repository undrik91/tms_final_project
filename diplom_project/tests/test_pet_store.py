import pytest
import allure
from diplom_project.pet_store_api.pet_store import PetStoreRest


response_ok = "<Response [200]>"


@allure.story("API test add pet")
def test_add_pet():
    api = PetStoreRest()
    api.post_pet(100, 0, "", "doggie", "", 0, "", "available")
    assert str(api.get_pet_id(100)) == response_ok


@pytest.mark.xfail()
@allure.story("API test delete pet")
def test_delete_pet():
    api = PetStoreRest()
    api.delete_pet(100)
    assert str(api.get_pet_id(100)) == response_ok


@allure.story("API test create user")
def test_create_user():
    api = PetStoreRest()
    api.post_user(5, "Oleg", "", "", "", "", "", 0)
    assert str(api.get_user("Oleg")) == response_ok


@allure.story("API test rename user")
def test_rename_user():
    api = PetStoreRest()
    api.put_user(5, "Dima", "", "", "", "", "", 0)
    assert str(api.get_user("Dima")) == response_ok
