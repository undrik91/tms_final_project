import allure
from diplom_project.pages.regional_page import RegionalPage


@allure.story("TC_1_currency")
def test_regional_currency(driver):
    with allure.step('open app'):
        button = RegionalPage(driver)
        button.open_page()
    with allure.step('click Regional settings'):
        button.open_regional_button()
        driver.implicitly_wait(10)
    with allure.step('select currency'):
        button.currency_button()
    with allure.step('save Regional settings and check ui'):
        button.save_button()
        assert button.ui_currency().text == "EUR"


@allure.story("TC_1_country")
def test_regional_country(driver):
    with allure.step('open app'):
        button = RegionalPage(driver)
        button.open_page()
    with allure.step('click Regional settings'):
        button.open_regional_button()
        driver.implicitly_wait(10)
    with allure.step('select country'):
        button.country_button()
    with allure.step('save Regional settings and check ui'):
        button.save_button()
        assert button.ui_country().text == "Angola"
